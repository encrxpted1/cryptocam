package com.tnibler.cryptocam.preference

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.tnibler.cryptocam.R

class LicensesFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.licenses, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val inStream = (requireActivity()).assets.open("licenses.txt")
        val text = inStream.readBytes().decodeToString()
        view.findViewById<TextView>(R.id.licensesView).text = text
    }
}