package com.tnibler.cryptocam

enum class Orientation {
    PORTRAIT, LAND_LEFT, LAND_RIGHT
}