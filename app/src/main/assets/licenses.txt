===Icon licenses===

Folder icon by Papirus Team:
https://github.com/PapirusDevelopmentTeam/

Key icon by Smashicons:
https://www.flaticon.com/authors/smashicons

===Library licenses===

WhatTheStack (Apache license v2):
https://github.com/haroldadmin/WhatTheStack

age (BSD 3-Clause):
https://github.com/FiloSottile/age

Code scanner (MIT license):
https://github.com/yuriy-budiyev/code-scanner

Apache Commons Coded (Apache license v2):
http://commons.apache.org/proper/commons-codec/

ZXing (Apache license v2):
https://github.com/zxing/zxing